#include <string>
#include <random>

using namespace std;
using std::string;

string randDNA(int seed, string bases, int n)
{
	string finalString{""};//The string that will be filled with the random letters
	mt19937 eng1(seed);//
	
	uniform_int_distribution<int> uniform(0, bases.size()-1);
	
	for(int i = 0; i < n; i++)
	{
		int k = uniform(eng1);
		
		finalString += bases[k];
	}
	
	return finalString;
}
